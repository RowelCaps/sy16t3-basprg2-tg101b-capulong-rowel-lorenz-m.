#pragma once
#include <string>
#include "Item.h"

using namespace std;

class RarityItem : public Item
{
public:
	RarityItem(string name, int val, float chance);
	~RarityItem();

	void useItem(Unit *actor);

private:
	int mValue;
};

