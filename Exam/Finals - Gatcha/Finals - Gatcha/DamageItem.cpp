#include "DamageItem.h"
#include "Unit.h"

DamageItem::DamageItem(string name, int damage, float chance)
{
	mDamage = damage;
	this->setName(name);
	this->setChance(chance);
}

void DamageItem::useItem(Unit * actor)
{
	Item::useItem(actor);

	actor->useDamageItem(mDamage);
	actor->subCrystal(5);
	actor->pullItem();

	cout << "You received " << mDamage << " damage" << endl << endl;
}

DamageItem::~DamageItem()
{
}
