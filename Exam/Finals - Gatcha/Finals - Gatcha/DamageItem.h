#pragma once
#include<string>
#include "Item.h"

using namespace std;

class DamageItem : public Item
{
public:
	DamageItem(string name, int damage, float chance);

	void useItem(Unit * actor);
	~DamageItem();

private:
	int mDamage;
};

