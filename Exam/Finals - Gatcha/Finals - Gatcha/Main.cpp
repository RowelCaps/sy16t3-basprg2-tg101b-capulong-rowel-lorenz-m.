#include <iostream>
#include "Unit.h"
#include <time.h>
#include "HealItem.h"
#include "DamageItem.h"
#include "RarityItem.h"
#include "CrystalItem.h"
#include <string>

using namespace std;

vector<Item*> createItems();
Item * pullItem(vector<Item*> items);
int getSumChance(vector<Item*> items);
void printPulledItems(vector<Item*> items, Unit *actor);

int main()
{
	srand((unsigned)time(NULL));

	Unit *player = new Unit(100, 100);
	vector<Item*> availablePull = createItems();
	Item *pulledItem = NULL;

	while (player->isAlive() && player->isWin() && player->noCrystal())
	{
		player->printInfo();
		pulledItem = pullItem(availablePull);
		player->addItems(pulledItem);
		pulledItem->useItem(player);

		system("pause");
		system("cls");
	}

	if (!player->isWin()) cout << "You win! \n\n";
	else cout << "You lose! \n\n";

	cout << "=====================\n";
	player->printInfo();
	cout << "=====================\n";
	printPulledItems(availablePull, player);

	delete player;
	availablePull.clear(); // <<--- according to stack overflow it deletes the whole vector

	system("pause");
	return 0;
}

vector<Item*> createItems()
{
	vector<Item*> itemAvailable;

	Item * healPotion = new HealItem("Heal Potion", 30, 15);
	itemAvailable.push_back(healPotion);

	Item * bomb = new DamageItem("Bomb", 25, 20);
	itemAvailable.push_back(bomb);

	Item *crystal = new CrystalItem("Crystal", 15, 15);
	itemAvailable.push_back(crystal);

	Item * R = new RarityItem("R", 1, 40);
	itemAvailable.push_back(R);

	Item * SR = new RarityItem("SR", 10, 9);
	itemAvailable.push_back(SR);


	Item * SSR = new RarityItem("SSR", 50, 1);
	itemAvailable.push_back(SSR);

	return itemAvailable;
}

Item * pullItem(vector<Item*> item)
{
	int itemPulled = 0;
	float sumChance = (float)getSumChance(item);
	unsigned int pullChance = 100; // <<-- pullChance represents the percentage of sumChance
	int probability = 0;

	for (int i = 0; pullChance > 0 ;i++)
	{
		i = i > static_cast<int>(item.size()) ? item.size() : i; // <<- to prevent the vector going out of scope. this is actually not needed but i put it just in case.

		probability = (rand() % pullChance) + 1;
		float itemChance = (item[i]->getChance() / sumChance) * 100; // <-- to increase the chance of successful pulls for the next items

		if (probability <= itemChance) // <-- to retain the probability of the item getting pulled, put item[i]->getChance().
		{                                        //     to adjust the probability of the item automatically, put itemChance.
			itemPulled = i;
			break;
		}

		pullChance -= (int)itemChance;
	}

	return item[itemPulled];
}

int getSumChance(vector<Item*> items)
{
	int sum = 0;

	for (size_t i = 0; i < items.size(); i++)
	{
		sum += items[i]->getChance();
	}

	return sum;
}

void printPulledItems(vector<Item*> items, Unit * actor)
{
	struct itemsPulled
	{
		string name;
		int quantity = 0;
	};

	vector<itemsPulled*>availableItems;
	vector<Item*>actorItems = actor->getItems();

	for (size_t i = 0; i < items.size(); i++)
	{
		itemsPulled * temp = new itemsPulled;
		temp->name = items[i]->getName();
		availableItems.push_back(temp);
	}

	for (size_t i = 0; i < actorItems.size(); i++)
	{
		for (size_t j = 0; j < availableItems.size(); j++)
		{
			if (actorItems[i]->getName() == availableItems[j]->name)
			{
				availableItems[j]->quantity++;
			}
		}
	}

	cout << "Items Pulled: \n----------------------------\n";

	for (size_t i = 0; i < availableItems.size(); i++)
	{
		if (availableItems[i]->quantity != 0)
		cout << availableItems[i]->name << " x" << availableItems[i]->quantity << endl;
	}

	availableItems.clear();
}

