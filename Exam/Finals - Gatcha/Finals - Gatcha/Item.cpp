#include "Item.h"
#include "Unit.h"
#include <iostream>

Item::Item()
{
}


Item::~Item()
{
}

void Item::useItem(Unit * actor)
{
	cout << "Pulled a " << mName << endl;
}

void Item::setName(string name)
{
	mName = name;
}

void Item::setChance(float chance)
{
	mChance = chance;
}

string Item::getName()
{
	return mName;
}

int Item::getChance()
{
	return mChance;
}

