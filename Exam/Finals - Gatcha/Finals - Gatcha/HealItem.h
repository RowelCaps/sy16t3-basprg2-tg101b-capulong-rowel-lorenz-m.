#pragma once
#include<string>
#include "Item.h"

using namespace std;

class HealItem : public Item
{
public:
	HealItem(string name, int healVal, float chance);

	void useItem(Unit *actor);

	~HealItem();

private:
	int mHealValue;
};

