#pragma once
#include<string>

using namespace std;

class Unit;

class Item
{
public:
	Item();
	~Item();

	virtual void useItem(Unit * actor);
	void setName(string name);
	void setChance(float chance);
	string getName();
	int getChance();


private:
	string mName;
	Unit * actor;
	float mChance;
};

