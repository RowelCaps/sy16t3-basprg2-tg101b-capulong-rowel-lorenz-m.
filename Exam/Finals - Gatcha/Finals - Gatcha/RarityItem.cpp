#include "RarityItem.h"
#include "Unit.h"


RarityItem::RarityItem(string name, int val, float chance)
{
	mValue = val;
	this->setName(name);
	this->setChance(chance);
}


RarityItem::~RarityItem()
{
}

void RarityItem::useItem(Unit * actor)
{
	Item::useItem(actor);

	actor->addRarityPoints(mValue);
	actor->subCrystal(5);
	actor->pullItem();

	cout << "You received " << mValue << " Rarity Points! " << endl << endl;
}
