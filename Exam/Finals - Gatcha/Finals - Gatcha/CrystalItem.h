#pragma once
#include <string>
#include "Item.h"

using namespace std;

class CrystalItem : public Item
{
public:
	CrystalItem(string name, int val, float chance);

	void useItem(Unit * actor);

	~CrystalItem();

private:
	int mValue;
};

