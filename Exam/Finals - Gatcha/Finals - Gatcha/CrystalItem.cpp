#include "CrystalItem.h"
#include "Unit.h"

CrystalItem::CrystalItem(string name, int val, float chance)
{
	mValue = val;
	this->setName(name);
	this->setChance(chance);
}

void CrystalItem::useItem(Unit * actor)
{
	Item::useItem(actor);

	actor->addCrystal(mValue);
	actor->subCrystal(5);
	actor->pullItem();

	cout << "You received " << mValue << " crystals! " << endl << endl;
}

CrystalItem::~CrystalItem()
{

}
