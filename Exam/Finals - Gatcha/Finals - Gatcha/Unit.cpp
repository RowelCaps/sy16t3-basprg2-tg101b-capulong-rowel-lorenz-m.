#include "Unit.h"



Unit::Unit()
{
	mHp = 100;

	mUnitInfo.crystals = 100;
	mUnitInfo.rarityPoints = 0;
	mUnitInfo.pulls = 0;
}

Unit::Unit(int hp, int crystal)
{
	mHp = hp;

	mUnitInfo.crystals = crystal;
	mUnitInfo.rarityPoints = 0;
	mUnitInfo.pulls = 0;
}

void Unit::useHealItem(int hp)
{
	mHp += hp;
}

void Unit::useDamageItem(int damage)
{
	mHp -= damage;

	mHp = mHp < 0 ? 0 : mHp;
}

void Unit::addCrystal(int value)
{
	mUnitInfo.crystals += value;
}

void Unit::addItems(Item * pulled)
{
	mItems.push_back(pulled);
}

void Unit::pullItem()
{
	mUnitInfo.pulls++;
}

void Unit::addRarityPoints(int val)
{
	mUnitInfo.rarityPoints += val;
}

void Unit::subCrystal(int value)
{
	mUnitInfo.crystals -= value;

	mUnitInfo.crystals = mUnitInfo.crystals < 0 ? 0 : mUnitInfo.crystals;
}

void Unit::printInfo()
{
	cout << "HP: " << mHp << endl;
	cout << "Crystals: " << mUnitInfo.crystals << endl;
	cout << "Rarity Points: " << mUnitInfo.rarityPoints << endl;
	cout << "Pulls : " << mUnitInfo.pulls << endl << endl;
}

vector<Item*>& Unit::getItems()
{
	return mItems;
}

bool Unit::isAlive()
{
	bool alive = false;

	alive = mHp != 0? true : false; 

	return alive;
}

bool Unit::noCrystal()
{
	bool checkCrystal = false;

	checkCrystal = mUnitInfo.crystals != 0 ? true : false;

	return checkCrystal;

}

bool Unit::isWin()
{
	bool win = false;

	win = mUnitInfo.rarityPoints < 100 ? true : false;

	return win;
}


Unit::~Unit()
{
	
}
