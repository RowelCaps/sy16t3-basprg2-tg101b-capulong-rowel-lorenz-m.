#include "HealItem.h"
#include"Unit.h"

HealItem::HealItem(string name, int healVal, float chance)
{
	this->setName(name);
	mHealValue = healVal;
	this->setChance(chance);
}

void HealItem::useItem(Unit * actor)
{
	Item::useItem(actor);
	actor->useHealItem(mHealValue);
	actor->subCrystal(5);
	actor->pullItem();

	cout << "You are healed for " << mHealValue << endl;
}


HealItem::~HealItem()
{
}
