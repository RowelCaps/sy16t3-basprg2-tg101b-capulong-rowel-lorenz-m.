#pragma once
#include<string>
#include<iostream>
#include"Item.h"
#include<vector>

using namespace std;

struct unitInfo // I'm having trouble coming up with a name this is the best I could think of.
{
	int crystals;
	int rarityPoints = 0;
	int pulls = 0;
};

class Unit
{
public:
	Unit();
	Unit(int hp, int crystal);

	void useHealItem(int hp);
	void useDamageItem(int damage);
	void addCrystal(int value);

	void addItems(Item *pulled); //it add the item you currently pulled
	void pullItem();
	void addRarityPoints(int val);
	void subCrystal(int value);
	void printInfo();
	vector<Item*>& getItems();

	bool isAlive();
	bool noCrystal();
	bool isWin();
	
	~Unit();

private:
	unitInfo mUnitInfo;
	vector<Item*>mItems;
	int mHp;
};

