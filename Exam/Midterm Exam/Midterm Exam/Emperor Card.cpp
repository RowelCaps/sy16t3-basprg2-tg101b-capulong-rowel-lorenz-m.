#include<iostream>
#include<string>
#include<time.h>

using namespace std;

struct cards
{
	string card;
	cards *next;
	cards *prev;
};

struct status
{
	int cash = 0;
	int distanceLeft = 30;
	int round = 1;
	int bet = 0;
	string side;
};

int startRound(status &player);
cards *giveCards(string cardType[3], string &side);
void printCards(cards *head, int size);
string turnOrder(status player);
cards *checkPickCards(cards *enemy, int cardSize, int pickCard);
string evaluateCards(cards *kaiji, cards *enemy);
void wipeCards(cards **head, int cardSize);
void delCards(cards *cardPicked, cards **head, int cardSize);
void payout(string result, status & player);
void evaluateEnding(status player);

int main()
{
	srand((unsigned)time(NULL));

	string cardType[3] = { "Emperor", "Citizen", "Slave"};
	string enemySide;
	status player;
	cards *headKaiji = NULL, *headEnemy = NULL; // to have access of the dynamically allocated cards for the 2 entity.

	while (player.distanceLeft > 0 && player.round <= 12)
	{
		cards * enemyCard = NULL, *playerCard = NULL; //this struct pointer is the card picked by player & enemy
		int playerPick, cardSize = 5;
		string result; // this is to check the round if there is a winner in a round
		
		player.bet = 0; //the bet of the player will always reset to 0 when the round starts

		player.side = turnOrder(player);
		player.bet = startRound(player);

		enemySide = player.side == "Emperor" ? "slave" : "Emperor";

		headKaiji = giveCards(cardType, player.side);
		headEnemy = giveCards(cardType, enemySide);

		system("cls");

		while (result != "you won" && result != "you lost")
		{
			cout << "Pick a card to play....\n";
			cout << "============================================\n\n";

			printCards(headKaiji, cardSize);

			cout << "\n";
			cin >> playerPick;

			int enemyPick = rand() % cardSize + 1;

			enemyCard = checkPickCards(headEnemy, cardSize, enemyPick);
			playerCard = checkPickCards(headKaiji, cardSize, playerPick);

			system("cls");

			result = evaluateCards(playerCard, enemyCard);

			delCards(enemyCard, &headEnemy, cardSize);
			delCards(playerCard, &headKaiji, cardSize);

			payout(result, player);
			cardSize--;

			system("pause");
			system("cls");
		}

		player.round++;
		wipeCards(&headKaiji, cardSize);
		wipeCards(&headEnemy, cardSize);

		system("cls");
	}

	evaluateEnding(player);
	system("pause");
}

int startRound(status &player) //this function is when the round started & won't allow the player to bet less than 0 or greater than distanceLeft
{
	cout << "Cash: " << player.cash << endl;
	cout << "Distance Left (mm): " << player.distanceLeft << endl;
	cout << "Round: " << player.round << endl;
	cout << "Side: " << player.side << "\n\n";

	while (player.bet <= 0 || player.bet > player.distanceLeft)
	{
		cout << "How many millimeter do you want to bet? ";
		cin >> player.bet;
	}

	return player.bet;
}

cards *giveCards(string cardType[3], string &side)
{
	cards *head = NULL, *current = NULL, *temp = NULL;

	for (int i = 1; i <= 5; i++)
	{
		int cardNum = 1;

		if (i == 1) cardNum = side == "Emperor" ? 0 : 2;

		temp = new cards();
		temp->card = cardType[cardNum];
		temp->next = NULL;
		temp->prev = NULL;

		if (head == NULL) head = temp;
		else
		{
			current = head;
			while (current->next != NULL)
			{
				current = current->next;
			}

			temp->prev = current;
			current->next = temp;
		}
	}

	temp->next = head;
	head->prev = temp;

	return head;
}

void printCards(cards *head, int cardSize)
{
	cards * run = head;

	for (int i = 1; i <= cardSize; i++)
	{
		cout << "[" << i << "] " << run->card << endl;
		run = run->next;
	}
}

string turnOrder(status player)
{
	if (player.round == 1) player.side = "Emperor";  

	else if (player.round % 3 == 1)
	{
		player.side = player.side == "Emperor" ? "Slave" : "Emperor";
	}

	return player.side;
}

cards *checkPickCards(cards *head, int cardSize, int pickCard) //this function returns a pointer to the card selected by the player or AI
{
	cards *chosenCard = head;

	for (int i = 1; i < pickCard; i++)
	{
		chosenCard = chosenCard->next;
	}

	return chosenCard;
}

string evaluateCards(cards *kaiji, cards *enemy)
{
	cout << "Show your cards! \n\n";
	cout << "[kaiji] " << kaiji->card << " vs " << "[Tonegawa] " << enemy->card << "\n\n\n";

	if (kaiji->card == "Emperor" && enemy->card == "Citizen") return "you won";

	else if (kaiji->card == "Slave" && enemy->card == "Emperor") return "you won";

	else if (kaiji->card == "Citizen" && enemy->card == "Slave") return "you won";

	else if (kaiji->card == enemy->card) return "Draw";

	else return "you lost";
}

void wipeCards(cards **head, int cardSize) // this is to prevent memory leak
{
	cards *run = *head, *temp = NULL;

	for (int i = 1; i <= cardSize; i++)
	{
		temp = run->next;
		delete run;
		run = temp;
	}
}

void delCards(cards *cardPicked, cards **head, int cardSize)
{
	if (cardSize == 2)
	{
		if (*head == cardPicked)
		{
			*head = (*head)->next;
			delete cardPicked;
		}
		else delete cardPicked;

		(*head)->prev = NULL;
		(*head)->next = NULL;
	}

	else if (cardSize == 1)
	{
		delete *head;
	}

	else
	{
		cards *right = cardPicked->next;
		cards *left = cardPicked->prev;

		right->prev = left;
		left->next = right;

		if (cardPicked == *head) *head = (*head)->next; // so I can wipe the whole linked list later;
		delete cardPicked;
	}
}

void payout(string result, status &player)
{
	int win;

	if (result == "you won")
	{
		win = player.bet * 100000;

		if (player.side == "Slave")
		{
			win *= 5;
		}

		cout << "you won " << win << " yen\n\n";
		player.cash += win;
	}

	else if (result == "Draw")
	{
		cout << result << "! \n\n";
	}

	else
	{
		player.distanceLeft -= player.bet;

		cout << "you lost! the pin will now move " << player.bet << " mm \n";
		cout << "Kaiji: Noooooooooooooooo! \n\n";
		cout << "*zap* *zap* *zap* \n\n";
	}
}

void evaluateEnding(status player)
{
	if (player.cash >= 20000000 && player.distanceLeft > 0)
	{
		cout << "You got the best ending! \n\n\n";
		cout << "you reach 20,000,000 yen! \n";
		cout << "Tonegawa kneels infront of you and ask for forgivenes...\n";
		cout << "The people who died may now rest in peace.... \n";
	}

	else if (player.cash < 20000000 && player.distanceLeft > 0)
	{
		cout << "You failed to reach 20,000,000 yen...\n\n";
		cout << "What is money if the people who died did not get justice because of your incompetence!...\n";
		cout << "Tonegawa keeps laughing until you left....\n";
	}

	else
	{
		cout << "The driller has peirced your eardrum...\n\n";
		cout << "Kaiji has loss and has failed to get justice for those who have fallen...\n";
		cout << "Kaiji: What?! I CAN'T HEAR YOU. \n";
		cout << "atleast, Kaiji will learn how to cast a jutsu.\n\n";
	}
}
