#include<iostream>
#include<string>
#include<time.h>

using namespace std;

void arrayPointer(int *value, int size)
{
	for (int i = 0; i < size; i++)
	{
		value[i] = rand() % 100 + 1;
	}
}

void printArray(int array[], int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << array[i] << endl;
	}
}

int main()
{
	srand(time(NULL));
	int arraySize;

	cout << "please enter the size of the array:";
	cin >> arraySize;

	int *numbers = new int[arraySize];

	arrayPointer(numbers, arraySize);
	printArray(numbers, arraySize);

	delete[] numbers;

	system("pause");
}