#include<iostream>
#include<string>
#include<time.h>


using namespace std;

struct item
{
	string name;
	int gold;
};

struct player
{
	int gold = 50;
	int goldMultiplier = 0;
	int potentialGold = 0;
};

item *generateItem()   // 1 = mithril ore; 2 = Sharp Talong; 3 = thick leather; 4 = jellopy; 5 = Cursed stond
{
	item *lootItem = new item();
	int probability = rand() % 5 + 1;


	if (probability == 1)
	{
		lootItem->name = "Mithril Ore";
		lootItem->gold = 100;
	}

	else if (probability == 2)
	{
		lootItem->name = "Sharp Talon";
		lootItem->gold = 50;
	}

	else if (probability == 3)
	{
		lootItem->name = "Thick Leather";
		lootItem->gold = 25;
	}

	else if (probability == 4)
	{
		lootItem->name = "Jellopy";
		lootItem->gold = 5;
	}
	else
	{
		lootItem->name = "Cursed stone";
		lootItem->gold = 0;
	}

	return lootItem;
}

void enterDungeon(player &player)
{
	if (player.goldMultiplier < 1)   //this bool is to check if we deduct the dungeon fee to the player's gold
	{
		cout << player.gold << "(currentgold) - 25(dungeon fee)" << endl;
		cout << "You entered the dungeon... \n\n\n\n" << endl;
		player.gold -= 25;
	}

	string playerResponse;
	item *loot = new item();

	while (playerResponse != "N" && playerResponse != "n")
	{
		loot = generateItem();
		player.goldMultiplier++;
		cout << "================================================ \n";

		player.potentialGold += (loot->gold * player.goldMultiplier);

		cout << "Gold: " << player.gold << endl;;
		cout << "Potential Gold: " << player.potentialGold << endl;
		cout << "You looted a " << loot->name << "!" << endl;
		cout << " Gold: " << loot->gold << endl;

		if (loot->name == "Cursed stone")
		{
			cout << "Oh no! you looted a Cursed stone! \n";
			cout << "You died.......\n";
			player.potentialGold = 0;
			break;
		}

		cout << "Continue looting? [yes]-enter any key | [no] - enter (n) or (N) :";
		cin >> playerResponse;

	}

	if (playerResponse == "n" || playerResponse == "N")
	{
		cout << "\n\n You went out of the dungeon....\n";
	}

	cout << "====================================\n\n";
	player.goldMultiplier = 0;
	player.gold += player.potentialGold;
	player.potentialGold = 0;
	system("pause");
	system("cls");
}

int main()
{
	srand((unsigned int)time(NULL));

	player player;

	while (player.gold >= 25 && player.gold < 499)
	{
		enterDungeon(player);

		cout << "current gold: " << player.gold << endl;
	}

	system("cls");

	if (player.gold < 25)
	{
		cout << "not enough gold to enter the dungeon\n";
		cout << "game over.....\n";
	}
	else
	{
		cout << "you reached the target gold \n";
		cout << "You win........\n";
	}

	system("pause");
}
