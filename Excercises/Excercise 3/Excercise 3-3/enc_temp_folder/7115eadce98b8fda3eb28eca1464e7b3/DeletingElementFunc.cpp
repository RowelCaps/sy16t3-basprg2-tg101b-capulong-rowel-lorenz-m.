#include<iostream>
#include<string>
#include<time.h>

using namespace std;

int** createArray(int size)
{
	int **newArray = new int*[size];
	
	for (int i = 0; i < size; i++)
	{
		newArray[i] = new int;
		 *newArray[i] = rand() % 100 + 1;
	}

	int **arrReturn = newArray;
	delete newArray[0];
	delete[] newArray;

	return arrReturn;
}

void printArray(int **array, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << *array[i] << endl;
	}
}

int main()
{
	srand(time(NULL));
	int arraySize;

	cout << "please enter array size:";
	cin >> arraySize;

	int **arrayPointer = createArray(arraySize);

	printArray(arrayPointer, arraySize);

	system("pause");
}