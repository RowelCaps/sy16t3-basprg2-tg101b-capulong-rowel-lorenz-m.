#include "Rectangle.h"
#include "Shape.h"
#include <iostream>



Rectangle::Rectangle(int width, int height)
{
	mWidth = width;
	mHeight = height;
	setName("Rectangle");
}


Rectangle::~Rectangle()
{
}

void Rectangle::findArea()
{
	area = mWidth * mHeight;
}

void Rectangle::printShape()
{
	cout << "Width = " << mWidth << endl;
	cout << "Length = " << mHeight << "\n\n";
}
