#include "Circle.h"
#include<iostream>

Circle::Circle(int radius)
{
	setName("Circle");

	mRadius = radius;
}

Circle::~Circle()
{
}

void Circle::findArea()
{
	area = 3.14f * mRadius;
}

void Circle::printShape()
{
	cout << "Radius: " << mRadius << "\n\n";
}
