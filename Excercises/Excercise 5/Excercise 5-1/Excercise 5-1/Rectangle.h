#pragma once
#include"Shape.h"

using namespace std;

class Rectangle : public Shape
{
public:
	Rectangle(int width,int height);
	~Rectangle();

	void findArea();
	void printShape();

private:
	int mWidth, mHeight;
};

