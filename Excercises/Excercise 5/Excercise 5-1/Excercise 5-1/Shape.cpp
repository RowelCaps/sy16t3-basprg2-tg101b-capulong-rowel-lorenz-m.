#include "Shape.h"
#include <iostream>

using namespace std;

Shape::Shape()
{
}


Shape::~Shape()
{
}

void Shape::printShape()
{
	cout << "Shape: " << mName << endl;
	cout << "Area: " << area << endl;
}

void Shape::setName(string name)
{
	mName = name;
}

int Shape::getArea()
{
	return area;
}

string Shape::getName()
{
	return mName;
}
