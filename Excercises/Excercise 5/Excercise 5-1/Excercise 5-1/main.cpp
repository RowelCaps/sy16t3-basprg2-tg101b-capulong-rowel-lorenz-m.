#include"Circle.h"
#include"Rectangle.h"
#include"Shape.h"
#include"Square.h"
#include <iostream>
#include<string>
#include<vector>

using namespace std;

int main()
{
	vector<Shape *>test;
	Circle * circle = new Circle(5);
	test.push_back(circle);

	Square * square = new Square(4);
	test.push_back(square);

	Rectangle *rectangle = new Rectangle(4, 6);
	test.push_back(rectangle);

	for (int i = 0; i < test.size(); i++)
	{
		Shape *temp = test[i];
		int area;
		string name = temp->getName();
		cout << "Shape :" << name << endl;

		temp->findArea();
		temp->printShape();
		area = temp->getArea();

		cout << "Area :" << area << endl;
		cout << endl;
	}

	system("pause");

	
}