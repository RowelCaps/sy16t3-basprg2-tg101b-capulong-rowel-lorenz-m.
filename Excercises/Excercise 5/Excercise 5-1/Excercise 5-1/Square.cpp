#include "Square.h"
#include "Shape.h"
#include<iostream>


Square::Square(int sides)
{
	mSides = sides;
	setName("Square");
}

Square::~Square()
{
}

void Square::findArea()
{
	area = mSides * mSides;
}

void Square::printShape()
{
	cout << "Sides: " << mSides << "\n\n";
}
