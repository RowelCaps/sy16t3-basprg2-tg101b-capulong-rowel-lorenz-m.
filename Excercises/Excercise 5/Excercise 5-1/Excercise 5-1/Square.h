#pragma once
#include"Shape.h"

using namespace std;

class Square : public Shape
{
public:
	Square(int sides);
	~Square();

	void findArea();
	void printShape();

private:
	int mSides;
};

