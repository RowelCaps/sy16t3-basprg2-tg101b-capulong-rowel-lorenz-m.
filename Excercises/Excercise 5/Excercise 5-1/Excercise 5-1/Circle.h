#pragma once
#include"Shape.h"
class Circle : public Shape
{
public:
	Circle(int radius);
	~Circle();

	void findArea();
	void printShape();


private:
	int mRadius;
};

