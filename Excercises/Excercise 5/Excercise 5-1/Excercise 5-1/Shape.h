#pragma once
#include<string>

using namespace std;

class Shape
{
protected:
	int area;

public:
	Shape();
	~Shape();

	virtual void findArea() = 0;
	virtual void printShape();
	void setName(string name);
	int getArea();
	string getName();

private:
	string mName;
};

