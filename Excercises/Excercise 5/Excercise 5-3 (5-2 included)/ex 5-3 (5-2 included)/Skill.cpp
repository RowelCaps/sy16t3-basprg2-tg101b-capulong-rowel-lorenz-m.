#include "Skill.h"
#include <iostream>
#include "Unit.h"

Skill::Skill()
{
}


Skill::~Skill()
{
}

void Skill::activateBuff(int skillNumber, Unit* actor)
{
	string useSkill = mSkills[skillNumber];
	baseStats temp = actor->getStats();
	cout << actor->getName();

	if (useSkill == "Heal")
	{
		actor->healUnit(10);
		cout << " cast heal!\n";
	}
	if (useSkill == "Might")
	{
		temp.power += 2;
		cout << " cast Might!\n";
	}

	if (useSkill == "Iron Skin")
	{
		temp.vitality += 2;
		cout << " cast Iron SKin!\n";
	}
	if (useSkill == "Concentration")
	{
		temp.dextirity += 2;
		cout << " cast Concentration\n";
	}

	if (useSkill == "Haste")
	{
		temp.agility += 2;
		cout << " cast Haste!\n";
	}

	actor->setStats(temp);
}

void Skill::setSkillUsed(int skill)
{
	mSkillUsed = mSkills[skill];
}

string Skill::getSkillUsed()
{
	return mSkillUsed;
}


