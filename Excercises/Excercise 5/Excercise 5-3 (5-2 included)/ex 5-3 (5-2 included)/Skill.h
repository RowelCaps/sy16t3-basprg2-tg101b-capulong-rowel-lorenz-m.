#pragma once
#include<string>

class Unit;

using namespace std;

class Skill
{
public:
	Skill();
	~Skill();

	void activateBuff(int skillNumber, Unit *actor);
	void setSkillUsed(int skill);
	string getSkillUsed();
private:
	string mSkills[5] = { "Heal","Might","Iron Skin", "Concentration","Haste" };
	string mSkillUsed;
};

