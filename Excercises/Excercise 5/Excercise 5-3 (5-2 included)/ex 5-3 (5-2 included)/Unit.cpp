#include "Unit.h"
#include<iostream>


Unit::Unit()
{
	mName = "Unit";
	mBaseStats.maxHp = 10;
	mBaseStats.power = 8;
	mBaseStats.vitality = 9;
	mBaseStats.dextirity = 6;
	mBaseStats.agility = 7;
	mCurrentHp = mBaseStats.maxHp;
}

Unit::Unit(string name)
{
	mName = name;
}


Unit::~Unit()
{
}

void Unit::healUnit(int plusHp)
{
	mCurrentHp += plusHp;
	mCurrentHp = mCurrentHp > mBaseStats.maxHp ? mBaseStats.maxHp : mCurrentHp;
}

baseStats  Unit::getStats()
{
	return mBaseStats;
}

void Unit::setStats(baseStats  increase)
{
	mBaseStats = increase;
}

void Unit::pushSkill()
{
	Skill *temp = new Skill();

	for (int i = 0; i < 5; i++)
	{
		temp->setSkillUsed(i);
		mSkill.push_back(temp);
	}
}

void Unit::useSKill(int skillNum)
{
	Skill *temp = mSkill[skillNum];
	temp->activateBuff(skillNum, this);
}

void Unit::printStats()
{
	cout << "Name: " << mName << endl;
	cout << "Hp: " << mCurrentHp << " / " << mBaseStats.maxHp << endl;
	cout << "Power: " << mBaseStats.power << endl;
	cout << "Vitality: " << mBaseStats.vitality << endl;
	cout << "Dextirity: " << mBaseStats.dextirity << endl;
	cout << "Agility: " << mBaseStats.agility << "\n\n";

}

string Unit::getName()
{
	return mName;
}
