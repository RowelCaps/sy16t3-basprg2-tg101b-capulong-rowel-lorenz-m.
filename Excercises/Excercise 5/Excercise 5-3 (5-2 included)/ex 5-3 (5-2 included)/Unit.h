#pragma once
#include<string>
#include<vector>
#include"Skill.h"

using namespace std;

struct baseStats
{
	int maxHp;
	int power;
	int vitality;
	int dextirity;
	int agility;
};

class Unit
{

public:
	Unit();
	Unit(string name);
	~Unit();

	void healUnit(int plusHp);
	baseStats getStats();
	void setStats(baseStats increase);
	void pushSkill();
	void useSKill(int skillNum);
	void printStats();
	string getName();
	 
private:
	string mName;
	vector<Skill*> mSkill;
	baseStats mBaseStats;
	int mCurrentHp;
};

