#include<iostream>
#include<string>
#include<time.h>

using namespace std;

void fillArray(int numArray[])
{
	for (int i = 0; i < 100; i++)
	{
		numArray[i] = (rand() % 100) + 1;
	}
}

void checkDuplicate(int numArray[])
{
	bool duplicateCheck = false;

	for (int i = 0; i < 100; i++)
	{
		int numDuplicate = 0;
		for (int j = i; j < 100; j++)
		{
			if (numArray[j] == numArray[i])
			{
				++numDuplicate;
				duplicateCheck = true;
			}
		}

		if (numDuplicate > 1)
		{
			cout << numArray[i] << " x " << numDuplicate << endl;
		}
	}

	if (duplicateCheck == false)
	{
		cout << "No containing identical values in the array" << endl;
	}
}

//This function is to print all values in the array to check PRINT_DUPLICATE_ARRAY
void printArray(int numArray[])
{
	for (int i = 0; i < 100; i++)
	{
		cout << numArray[i] << " ,";
	}

	cout << endl;
}

int main()
{
	srand(time(NULL));
	int numArray[100];

	fillArray(numArray);
	printArray(numArray);
	checkDuplicate(numArray);

	system("PAUSE");
	return 0;
}