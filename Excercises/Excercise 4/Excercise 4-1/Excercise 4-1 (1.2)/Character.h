#include<string>
#include"Weapon.h"

#pragma once

using namespace std;

struct Base
{
	int strength;
	int dexterity;
	int vititality;
	int magic;
	int spirit;
	int luck;
};

class Character
{
public:
	Character();
	Character(string name,Base Stats);

	void setBaseStats();
	int getMaxHp();
	int getMaxMp();
	int getAttack();
	int getDefense();
	int getMagic();

	Weapon *getWeapon();
	void equipWeapon();


	~Character();

private:

	string mName;
	unsigned int mLevel = 1; 
	int mExp;
	int mCurrentHp;
	int mCurrentMp;

	Base *baseStats;
	Weapon *mWeapon;
};


