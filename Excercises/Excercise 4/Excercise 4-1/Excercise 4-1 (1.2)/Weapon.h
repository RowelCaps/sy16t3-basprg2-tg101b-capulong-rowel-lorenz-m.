#pragma once

#include<string>

using namespace std;

class Weapon
{
public:
	Weapon();
	Weapon(string name, int attack);

	void setAttack(int damage);
	int getDamageWeapon();

	~Weapon();

private:
	string name;
	int attack;
};

