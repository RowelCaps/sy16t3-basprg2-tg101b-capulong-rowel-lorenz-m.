#include "Character.h"
#include"Weapon.h"


Character::Character()
{
	mName = "Cloud";
}

Character::Character(string name, Base Stats)
{
	mName = name;
	*baseStats = Stats;
}

int Character::getMaxHp()
{
	int hp = (baseStats->vititality / 4) + 1;
	return hp;
}

int Character::getMaxMp()
{
	int mp = (baseStats->magic / 4) + ((rand() % 6) + 1);
	return mp;
}

int Character::getAttack()
{
	int damage = baseStats->strength + 2;
	if (mWeapon != NULL) damage += mWeapon->getDamageWeapon;
	return damage;
}

int Character::getDefense()
{
	int def = baseStats->vititality + 2;
	return def;
}

int Character::getMagic()
{
	int magic = baseStats->magic + 5;
	return magic;
}

Weapon * Character::getWeapon()
{
	return mWeapon;

}


Character::~Character()
{
	mName = "Cloud";
}
