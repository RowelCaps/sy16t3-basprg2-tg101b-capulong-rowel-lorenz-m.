#include "Wizard.h"
#include "Spell.h"
#include <string>



Wizard::Wizard()
{
	mName = "Hairy Snotter";
}

Wizard::Wizard(string name)
{
	mName = name;
}

int Wizard::getMp()
{
	return mMp;
}

int Wizard::getHP()
{
	return mHp;
}

string Wizard::getName()
{
	return mName;
}

string Wizard::getSpellname()
{
	return mSpell->getSpellName();
}

void Wizard::castSpell(Wizard * player)
{
	mMp = mSpell->reduceMp(player);
}

void Wizard::spellHit(Wizard * enemy)
{
	enemy->mHp = mSpell->reduceMp(enemy);
}
