
#pragma once

using namespace std;

class Spell
{
public:
	Spell();
	~Spell();

	string getSpellName();
	int getDamage();
	int reduceMp(Wizard *wizard);
	int reduceHp(Wizard *wizard);

private:
	string mSpellName = "FireBall";
	int mDamage = 10;
	int mMpCost = 5;
};

