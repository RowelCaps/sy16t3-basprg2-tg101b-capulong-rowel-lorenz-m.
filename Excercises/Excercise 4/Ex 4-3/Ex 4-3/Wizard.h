#include "Spell.h"
#include <string>

#pragma once

using namespace std;

class Wizard
{
public:
	Wizard();
	Wizard(string name);
	~Wizard();

	int getMp();
	int getHP();
	string getName();
	string getSpellname();

	void castSpell(Wizard *player);
	void spellHit(Wizard *enemy);

private:
	string mName;
	int mHp = 100;
	int mMp = 100;
	Spell *mSpell;
};

