#include<string>
#include<iostream>
#include<time.h>
#include"Wizard.h"

using namespace std;

int main()
{
	srand((unsigned)time(NULL));
	Wizard *wizard1 = new Wizard();
	Wizard *wizard2 = new Wizard("Gandolfin");

	for (int i = 0; wizard1->getHP > 0 && wizard2->getHP > 0; i++)
	{
		cout << wizard1->getName << "\n HP:" << wizard1->getHP;
		cout << "\n MP:" << wizard1->getMp << "\n\n";


		cout << wizard2->getName << "\n HP:" << wizard2->getHP;
		cout << "\n MP:" << wizard2->getMp << "\n\n";

		if (i % 2 == 0)
		{
			cout << wizard1->getName << "'s turn!\n";

			wizard1->castSpell(wizard1);

			cout << wizard1->getName << " Cast a " << wizard1->getSpellname();
			wizard1->spellHit(wizard2);
		}
		else
		{
			cout << wizard2->getName << "'s turn!\n";

			wizard2->castSpell(wizard2);

			cout << wizard2->getName << " Cast a " << wizard2->getSpellname();
			wizard1->spellHit(wizard2);
		}

		system("pause");
	}

}