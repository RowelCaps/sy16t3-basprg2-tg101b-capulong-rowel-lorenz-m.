#include "Spell.h"
#include "Wizard.h"
#include<string>



Spell::Spell()
{
}


Spell::~Spell()
{
}

string Spell::getSpellName()
{
	return mSpellName;
}

int Spell::getDamage()
{
	return rand() % mDamage + 1;
}


int Spell::reduceMp(Wizard * wizard)
{
	int currentMp = (wizard->getHP) - mMpCost;
	return currentMp;
}

int Spell::reduceHp(Wizard * wizard, int damage)
{
	int currentHp = wizard->getHP - damage;
	return currentHp;
}
