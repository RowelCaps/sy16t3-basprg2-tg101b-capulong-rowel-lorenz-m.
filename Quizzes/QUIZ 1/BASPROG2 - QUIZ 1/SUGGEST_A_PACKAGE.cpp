#include<iostream>
#include<string>

using namespace std;

void sortPackage(int packages[], int size);
int suggestPackage(int itemPrice, int playerMoney, int packages[], int size);
int validPrice(int packages[], int itemPrice, int playerMoney, int size);
void playerShopping(int packages[], int itemPrice, int playerMoney, int size);

int main()
{
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int size = 6;
	int playerMoney = 250;
	int itemPrice = 0;
	bool continueShopping = true;

	sortPackage(packages, size);
	playerShopping(packages, itemPrice, playerMoney, size);

	return 0;
}

void sortPackage(int packages[], int size)
{
	for (int i = 0; i <= size; i++)
	{
		int smallestIndex = i;

		for (int j = smallestIndex; j <= size; j++)
		{
			if (packages[j] < packages[smallestIndex]) smallestIndex = j;
		}

		int prevNum = packages[i];
		packages[i] = packages[smallestIndex];
		packages[smallestIndex] = prevNum;
	}
}

//This function is to check the item price if it excedes the highest value in packages array
int validPrice(int packages[], int itemPrice, int playerMoney, int size)
{
	bool priceIsValid = false;
	while (!priceIsValid)
	{
		cout << "Enter item price: ";
		cin >> itemPrice;

		if (itemPrice > packages[size])
		{
			cout << "No available package to buy the item. Please enter the price again.." << endl;
		}
		else priceIsValid = true;
	}

	return itemPrice;
}

int suggestPackage(int itemPrice, int playerMoney, int packages[], int size)
{
	cout << "Insufficient gold to buy the item!" << endl;

	string playerResponse;
	int arrayNum;

	for (int i = 0; i <= size; i++)
	{
		if (itemPrice <= packages[i] + playerMoney) //to check if what packages does the player need to buy the item
		{
			arrayNum = i;
			break;
		}
	}

	bool validResponse = false;
	while (!validResponse)
	{
		cout << " Would you like to buy Package # " << arrayNum + 1 << "(" << packages[arrayNum] << " gold):";
		cin >> playerResponse;

		if (playerResponse == "yes" || playerResponse == "YES")
		{
			cout << endl << "Thank you for purchasing package #" << arrayNum + 1 << endl;
			return (playerMoney + packages[arrayNum]) - itemPrice;
			validResponse = true;
		}
		if (playerResponse == "no" || playerResponse == "NO")
		{
			return playerMoney;
			validResponse = true;
		}
	}
}

void playerShopping(int packages[], int itemPrice, int playerMoney, int size)
{
	bool continueShopping = true;

	while (continueShopping)
	{
		string playerResponse;
		cout << "Gold: " << playerMoney << endl;

		itemPrice = validPrice(packages, itemPrice, playerMoney, size);

		if (itemPrice > playerMoney)
		{
			playerMoney = suggestPackage(itemPrice, playerMoney, packages, size);
		}
		else
		{
			cout << "You bought an item!" << endl;
			playerMoney -= itemPrice;
		}

		cout << endl << "[No]- quit the program." << endl << "[enter any key] - continue the program" << endl;
		cout << "would you like to continue shopping?: ";
		cin >> playerResponse;

		if (playerResponse == "NO" || playerResponse == "no") continueShopping = false;

		system("cls");
	}
}