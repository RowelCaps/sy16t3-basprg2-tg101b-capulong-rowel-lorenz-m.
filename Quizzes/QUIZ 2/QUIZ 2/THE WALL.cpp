#ifndef NODE_H
#define NODE_H

#include <string>
#include<iostream>
#include<time.h>

using namespace std;

struct node
{
	string name;
	node* next = NULL;
	node* prev = NULL;
};

#endif 

node *enqueue(node *head, node *run, node *temp, string *members, int size)
{
	for (int i = 0; i < size; i++)
	{
		temp = new node();
		temp->name = members[i];
		temp->next = NULL;
		temp->prev = NULL;

		if (head == NULL) head = temp;
		else
		{
			run = head;
			while (run->next != NULL)
			{
				run = run->next;
			}
			temp->prev = run;
			run->next = temp;
		}
	}

	run = run->next;
	head->prev = run;  // when the loop ends run is now on the last node. this 3 lines of code is to connect the head and the last node
	run->next = head;

	return head;
}

string *addPlayer(int size)
{
	string *newPlayer = new string[size];

	for (int i = 0; i < size; i++)
	{
		cout << "enter your name soldier! :";
		cin >> newPlayer[i];
	}

	return newPlayer;
}

void startingPoint(node **head, node**run, int &playerDrawn, int size) //Function to set a starting point
{
	*run = *head;
	playerDrawn = rand() % size + 1;

	for (int i = 1; i <= playerDrawn; i++)
	{
		*run = (*run)->next;
	}

	*head = *run;
}

void deletePlayer(node** head, node* run, int playerDrawn)
{
	run = *head;

	for (int i = 0; i < playerDrawn; i++)
	{
		run = run->next;
	}

	if (run->next == *head  && run->prev == *head) //if there are only 2 nodes left and the cloak is not with the head. head will be set to NULL and run shall be deleted 
	{
		(*head)->next = NULL;
		(*head)->prev = NULL;
	}
	else if (run == *head) //if there are only 2 nodes left and the cloak is with the head. head will be set to the next node	
	{
		*head = (*head)->next;
		(*head)->next = NULL;
		(*head)->prev = NULL;
	}
	else
	{
		node *left = run->prev;
		node *right = run->next;

		left->next = right;
		right->prev = left;
		*head = run->next;
	}

	delete run;
}

void displayResults(node *head, node *run, int playerDrawn)
{
	run = head;

	for (int i = 1; i <= playerDrawn; i++)
	{
		run = run->next;
	}

	cout << "\n\n\nResult: \n";
	cout << head->name << " has drawn " << playerDrawn << endl;
	cout << run->name << " was eliminated \n\n";
}

void printNodes(node *head, node* run, int size)
{
	run = head;

	for (int i = 1; i <= size; i++)
	{
		cout << run->name << endl;
		run = run->next;
	}
}

int main()
{
	srand((unsigned)time(NULL));

	node *head = NULL, *run = NULL, *temp = NULL;
	int size;
	int playerDrawn;

	cout << "How many soldiers will participate? :";
	cin >> size;

	string *player = addPlayer(size);
	head = enqueue(head, run, temp, player, size);
	startingPoint(&head, &run, playerDrawn, size);

	for (int i = 1; head->next != NULL || head->prev != NULL; i++)
	{
		cout << "================================\n";
		cout << "ROUND " << i << endl;
		cout << "================================\n\n";

		printNodes(head, run, size);
		displayResults(head, run, playerDrawn);
		deletePlayer(&head, run, playerDrawn);
		size--;
		playerDrawn = rand() % size + 1;

		system("pause");
	}

	cout << "================================\n";
	cout << "FINAL RESULT\n";
	cout << "================================\n";
	cout << head->name << " will go seek for reinforcement\n";

	system("pause");
}