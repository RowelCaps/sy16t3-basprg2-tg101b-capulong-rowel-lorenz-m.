#include"Unit.h"
#include"Player.h"
#include"Enemy.h"
#include<iostream>
#include<string>
#include<time.h>

using namespace std;

int main()
{
	srand((unsigned)time(NULL));
	string name;
	int classNum, maxStage;


	cout << "Please Enter your name: ";
	cin >> name;

	system("cls");

	cout << "Pick a class...\n\n";
	cout << "[1] - Warrior \n[2] - Assassin \n[3] - Mage \n";
	cin >> classNum;

	system("cls");

	Player *player = new Player(name, classNum);

	for (int i = 1; player->getHp() > 0; i++)
	{
		Enemy *enemy = new Enemy();
		enemy->spawnEnemy();
		enemy->enemyGrow(i);

		cout << "Stage: " << i << endl;

		player->printStats();
		cout << endl;
		enemy->printStats();

		system("pause");
		system("cls");

		player->playerBattle(enemy);

		system("cls");

		if (player->getHp() > 0)
		{
			player->levelUp(enemy->getClass());
			player->healPlayer();

			system("pause");
		}

		system("cls");

		maxStage = i;
		delete enemy;
	}

	cout << "reached stage " << maxStage << endl;

	delete player;

	system("pause");
}