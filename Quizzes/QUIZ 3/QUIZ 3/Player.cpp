#include "Player.h"
#include "Enemy.h"
#include<string>
#include <iostream>

using namespace std;

Player::Player(string name, int classNum)
{
	string playerClass;

	if (classNum == 1) playerClass = "Warrior";
	if (classNum == 2) playerClass = "Assassin";
	if (classNum == 3) playerClass = "Mage";

	setName(name);
	setClass(playerClass);

	mBaseStats.vitality = rand() % 8 + 5;
	mBaseStats.agility = rand() % 8+ 4;
	mBaseStats.dextirity = rand() % 6 + 4;
	mBaseStats.power = rand() % 10 + 4;

	setMaxHp(rand() % 10 + mBaseStats.vitality);
}


Player::~Player()
{
	cout << "Player's stats...\n";
	printStats();
}

void Player::levelUp(string enemyClass)
{
	cout << "battle has ended....\n";
	cout << "Increased stats by...\n";
	if (enemyClass == "Warrior")
	{
		mBaseStats.maxHp += 3;
		mBaseStats.vitality += 3;

		cout << "\n\n Max Hp +3..\n";
		cout << "Vitality + 3\n";
		return;
	}
	if (enemyClass == "Assassin")
	{
		mBaseStats.agility += 3;
		mBaseStats.dextirity += 3;

		cout << "Agility + 3\n";
		cout << "dextirity + 3\n";
		return;
	}
	if (enemyClass == "Mage")
	{
		mBaseStats.power += 5;

		cout << "Power + 5\n";
		return;
	}
}

void Player::healPlayer()
{
	int heal;
	heal = mCurrentHp * 1.3f;

	cout << "\n\n\n You are healed by " << heal << " Hp...\n";
	mCurrentHp += heal;
	mCurrentHp = mCurrentHp > mBaseStats.maxHp ? mBaseStats.maxHp : mCurrentHp;
}

void Player::playerBattle(Unit * enemy)
{
	int playerDamage, enemyDamage, playerTurn;

	for (int i = 1; this->getHp() > 0 && enemy->getHp() > 0; i++)
	{
		if (this->getAgi() > enemy->getAgi() && i == 1)
		{
			playerTurn = 0;
			this->attack(enemy);
		}
		else if (i == 1)
		{
			playerTurn = 1;
			enemy->attack(this);
		}

		else
		{
			if (playerTurn)
			{
				this->attack(enemy);
			}

			else
			{
				enemy->attack(this);
			}

			playerTurn = playerTurn == 1 ? 0 : 1;
		}
	}

	if (mCurrentHp != 0) cout << enemy->getName() << " has died\n";
	else cout << this->getName() << " has died\n";

	system("pause");
}


