#pragma once
#include"Unit.h"

using namespace std;

class Enemy : public Unit
{
public:
	Enemy();
	~Enemy();

	void spawnEnemy();
	void enemyGrow(int timeStep);
	void resetHp();

};

