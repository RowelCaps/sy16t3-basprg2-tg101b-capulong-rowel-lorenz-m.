#pragma once

#include<string>

using namespace std;

struct Stats
{
	int maxHp;
	int power;
	int vitality;
	int agility;
	int dextirity;
};

class Unit
{
protected:
	Stats mBaseStats;
	int mCurrentHp;

public:
	Unit();
	~Unit();

	void setName(string name);
	void setClass(string name);

	void setMaxHp(int hp);
	int getHp();
	
	string getName();
	string getClass();

	int getVit();
	int getAgi();
	int getDex();
	bool checkHit(Unit *defender);

	void takeDamage(int dmg);
	void attack(Unit *defender);
	void printStats();

private:

	string mClass;
	string mName;
};



