#include "Unit.h"
#include<string>
#include<time.h>
#include<iostream>

using namespace std;


Unit::Unit()
{
}


Unit::~Unit()
{
}

void Unit::setName(string name)
{
	mName = name;
}

void Unit::setClass(string name)
{
	mClass = name;
}

void Unit::setMaxHp(int hp)
{
	mBaseStats.maxHp = hp;
	mCurrentHp = mBaseStats.maxHp;
}

int Unit::getHp()
{
	return mCurrentHp;
}

string Unit::getName()
{
	return mName;
}

string Unit::getClass()
{
	return mClass;
}

int Unit::getVit()
{
	return mBaseStats.vitality;
}

int Unit::getAgi()
{
	return mBaseStats.agility;
}

int Unit::getDex()
{
	return mBaseStats.dextirity;
}

bool Unit::checkHit(Unit *defender)
{
	float hitRate = ((float)getDex() / defender->getAgi()) * 100;

	if (hitRate < 20) return hitRate = 20;
	if (hitRate > 80) return hitRate = 80;

	if (rand() % 100 + 1 > hitRate) return false;
	
	return true;
}

void Unit::takeDamage(int dmg)
{
	if (mCurrentHp < dmg)
	{
		mCurrentHp = 0;
		return;
	}

	mCurrentHp -= dmg;
}

void Unit::attack(Unit *defender)
{
	float bonusDamage = 1.5f;
	int playerDamage;

	if (mClass == "Warrior" && defender->getClass() == "Mage") bonusDamage = 1.0f;
	if (mClass == "Assassin" && defender->getClass() == "Warrior") bonusDamage = 1.0f;
	if (mClass == "Mage" && defender->getClass() == "Assassin") bonusDamage = 1.0f;

	int damage = (mBaseStats.power - defender->getVit()) * bonusDamage;
	damage = damage <= 0 ? 1 : damage;

	playerDamage = rand() % damage +1;

	if (this->checkHit(defender))
	{
		defender->takeDamage(playerDamage);
		cout << this->getName() << " Dealt " << playerDamage << " damage!" << endl;
	}

	else cout << this->getName() << " missed!" << endl;

	system("pause");
	
}

void Unit::printStats()
{
	cout << "Name: " << getName() << endl;
	cout << "Class: " << getClass() << endl;
	cout << "HP: " << getHp() << " / " << mBaseStats.maxHp << endl;
	cout << "Pow:" << mBaseStats.power << endl;
	cout << "Vit: " << mBaseStats.vitality << endl;
	cout << "Dex: " << mBaseStats.dextirity << endl;
	cout << "Agi: " << mBaseStats.agility << endl;
}
