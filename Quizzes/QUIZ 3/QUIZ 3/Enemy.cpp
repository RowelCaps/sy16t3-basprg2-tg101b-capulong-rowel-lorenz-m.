#include "Enemy.h"
#include "Unit.h"
#include<string>

using namespace std;

Enemy::Enemy()
{
	setMaxHp(rand() % 10 + 1);

	mBaseStats.vitality = (rand() % 6 + 1);
	mBaseStats.agility = (rand() % 5 + 1);
	mBaseStats.dextirity = (rand() % 5 + 1);
	mBaseStats.power = (rand() % 6 + 1);

	if (getClass() == "Assassin") mBaseStats.agility += 3;
	else if (getClass() == "Mage") mBaseStats.power += 5;
	else mBaseStats.vitality += 1;
}


Enemy::~Enemy()
{
}

void Enemy::spawnEnemy() //it randomize the class of the enemy
{
	int classNum = rand() % 3 + 1;
	string Class;

	if (classNum == 1)
	{
		setClass("Warrior");
		setName("warrior");
	}

	if (classNum == 2)
	{
		setClass("Assassin");
		setName("Mage");
	}

	if (classNum == 3)
	{
		setClass("Mage");
		setName("Mage");
	}
}

void Enemy::enemyGrow(int timeStep)
{
	mBaseStats.maxHp += 3 * timeStep;        //I used linear progression and the amount of growth is all set to 3 so it will not be complicated
	mBaseStats.vitality += 3 * timeStep;
	mBaseStats.agility += 3 * timeStep;
	mBaseStats.dextirity += 3 * timeStep;
	mBaseStats.power += 3 * timeStep;

	mCurrentHp = mBaseStats.maxHp;
}

void Enemy::resetHp()
{
	mCurrentHp = mBaseStats.maxHp;
}
