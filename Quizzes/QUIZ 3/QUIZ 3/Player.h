#pragma once
#include "Unit.h"
#include "Enemy.h"

using namespace std;

class Player : public Unit
{
public:
	Player(string name, int classNum);
	~Player();

	void levelUp(string enemyClass);
	void healPlayer();
	void playerBattle(Unit *enemy);
};

